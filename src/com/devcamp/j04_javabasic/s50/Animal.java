package com.devcamp.j04_javabasic.s50;

public abstract class Animal {
    protected int age;
    protected String gender;

    public abstract void isMammal();
    public abstract void mate();
}
