package com.devcamp.j04_javabasic.s50;

public class Main {
    public static void main(String[] args) {
        Duck duck = new Duck(2, "male", "yellow");
        Fish fish = new Fish(1, "female", 3, true);
        Zebra zebra = new Zebra(3, "male", true);
        System.out.println("duck = " + duck);
        System.out.println("fish = " + fish);
        System.out.println("zebra = " + zebra);
    }
}
