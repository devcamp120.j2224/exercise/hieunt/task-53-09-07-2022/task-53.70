package com.devcamp.j04_javabasic.s50;

public class Fish extends Animal {
    // các thuộc tính của class Fish
    private int size;
    private boolean canEat;
    // method khởi tạo có đủ 4 tham số
    public Fish(int age, String gender, int size, boolean canEat) {
        this.age = age;
        this.gender = gender;
        this.size = size;
        this.canEat = canEat;
    }
    @Override
    public String toString() {
        return
        "{age: " + age + ", gender: \"" + gender +
        "\", size: " + size + ", canEat: " + canEat + "}";
    }
    // các method của class Fish
    @Override
    public void isMammal() {
        System.out.println("Fish is mammal");
    }
    @Override
    public void mate() {
        System.out.println("Fish mates");
    }
    public void swim() {
        
    }
    // các method set và get cho các thuộc tính của class Fish
    public void setAge(int age) {
        this.age = age;
    }
    public int getAge() {
        return this.age;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getGender() {
        return this.gender;
    }
    public void setSize(int size) {
        this.size = size;
    }
    public int getSize() {
        return this.size;
    }
    public void setCanEat(boolean canEat) {
        this.canEat = canEat;
    }
    public boolean getCanEat() {
        return this.canEat;
    }
}
