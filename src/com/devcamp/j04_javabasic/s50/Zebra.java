package com.devcamp.j04_javabasic.s50;

public class Zebra extends Animal {
    // các thuộc tính của class Zebra
    private boolean is_wild;
    // method khởi tạo có đủ 3 tham số
    public Zebra(int age, String gender, boolean is_wild) {
        this.age = age;
        this.gender = gender;
        this.is_wild = is_wild;
    }
    @Override
    public String toString() {
        return
        "{age: " + age + ", gender: \"" + gender
        + "\", is_wild: " + is_wild + "}";
    }
    // các method của class Zebra
    @Override
    public void isMammal() {
        System.out.println("Zebra is mammal");
    }
    @Override
    public void mate() {
        System.out.println("Zebra mates");
    }
    public void run() {
        
    }
    // các method set và get cho các thuộc tính của class Zebra
    public void setAge(int age) {
        this.age = age;
    }
    public int getAge() {
        return this.age;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getGender() {
        return this.gender;
    }
    public void setIsWild(boolean is_wild) {
        this.is_wild = is_wild;
    }
    public boolean getIsWild() {
        return this.is_wild;
    }
}
