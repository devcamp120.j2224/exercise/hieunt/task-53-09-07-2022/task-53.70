package com.devcamp.j04_javabasic.s50;

public class Duck extends Animal {
    // các thuộc tính của class Duck
    private String beakColor;
    // method khởi tạo có đủ 3 tham số
    public Duck(int age, String gender, String beakColor) {
        this.age = age;
        this.gender = gender;
        this.beakColor = beakColor;
    }
    @Override
    public String toString() {
        return
        "{age: " + age + ", gender: \"" + gender
        + "\", beakColor: \"" + beakColor + "\"}";
    }
    // các method của class Duck
    @Override
    public void isMammal() {
        System.out.println("Duck is mammal");
    }
    @Override
    public void mate() {
        System.out.println("Duck mates");
    }
    public void swim() {
        
    }
    public void quack() {
        
    }
    // các method set và get cho các thuộc tính của class Duck
    public void setAge(int age) {
        this.age = age;
    }
    public int getAge() {
        return this.age;
    }
    public void setGender(String gender) {
        this.gender = gender;
    }
    public String getGender() {
        return this.gender;
    }
    public void setBeakColor(String beakColor) {
        this.beakColor = beakColor;
    }
    public String getBeakColor() {
        return this.beakColor;
    }

}
